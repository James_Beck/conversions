﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conversions
{
    class Program
    {
        static void Main(string[] args)
        {
            var KilometerConv = 0.621371;
            var MileConv = 1.609344;
            var KiloToMile = 0.00;
            var MileToKilo = 0.00;

            ///Here I am communicating to the user
            Console.WriteLine("This program is to allow the user to convert values from Miles to Kilometers and vice versa");
            Console.WriteLine("Input a numerical value representing the kilometers you would like to convert into miles");
            ///This is the stored input number for Kilometers chosen by the user
            KiloToMile = int.Parse(Console.ReadLine());
            ///These lines are in response to the users input
            Console.WriteLine("Cool that is");
            Console.WriteLine(KiloToMile * KilometerConv);
            Console.WriteLine("miles");
            ///This is to ask the user for a value in miles
            Console.WriteLine("Now input a numerical value representing the miles you would like to convert into kilometer");
            ///The stored value in miles chosen by the user
            MileToKilo = int.Parse(Console.ReadLine());
            ///The response to their value in miles
            Console.WriteLine("Okay, that's");
            Console.WriteLine(MileToKilo * MileConv);
            Console.WriteLine("kilometers");
            
        }
    }
}
